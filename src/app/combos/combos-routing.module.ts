import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CombosComponent } from './combos/combos.component';

const routes: Routes = [
  {
    path: "combos",
    component: CombosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CombosRoutingModule { }
